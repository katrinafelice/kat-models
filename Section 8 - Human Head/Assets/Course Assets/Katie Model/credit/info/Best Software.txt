

Best Free Software Sites
1. www.alternativeto.net
2. www.filehippo.com
3. www.sourceforge.org
4. www.majorgeeks.com


COMMERCIAL PHOTO EDITORS
1. Photoshop  				http://www.adobe.com/downloads/
2. Lightroom 				http://www.adobe.com/downloads/
3. DxO OPTICS PRO 	 		http://www.dxo.com
4. Photo Brush   		 	http://www.mediachance.com/pbrush/
5. PhotoPlus 				http://www.serif.com/photoplus/
6. Photomatix 			 	http://www.hdrsoft.com
7. SageLight  				http://www.sagelighteditor.com/


FREE PHOTO EDITORS
1. Gimp 				http://www.gimp.org
2. Photo Scape 				http://www.photoscape.org/ps/main/index.php
3. Paint.net 			 	http://www.getpaint.net/download.html
4. Photofiltre  			http://photofiltre.en.softonic.com
5. LumaPix 				http://www.lumapix.com/index.shtml
6. Dark Table				

FREE PHOTO VIEWERS
1. Faststone 			 	http://www.faststone.org
2. Irfanview 				http://www.irfanview.com


ONLINE IMAGE EDITORS
1. Splash Up 			 	http://www.splashup.com
2. Flauntr 				http://www.flauntr.com
3. FotoFlexer 			 	http://fotoflexer.com


BEST COMMERCIAL PHOTOSHOP PLUGINS 
1. Topaz 			 	https://www.topazlabs.com/store/
2. Noiseware 			 	http://www.imagenomic.com/nw.aspx
3. Noise Ninja 			 	http://www.picturecode.com/
4. onOne Software 		  	http://www.ononesoftware.com/
5. Neat Image 			 	http://www.neatimage.com/
6. NIK Define 2.0  			https://www.google.com/nikcollection/products/dfine/


BEST FREE PHOTOSHOP PLUGINS
1. Colormancer Noise Reduction	 	http://www.colormancer.ca/free/
2. FFT filter   			http://www.retouchpro.com/
3. Ximagic Denoiser     	 	http://www.ximagic.com/d_index.html
4. Hotpixel 			 	http://www.redpawmedia.com/hotpixel.html


CAMERA AND LENS INFO
1. Digital Camera World			http://www.digitalcameraworld.com
2. Digital Photo Review 		http://www.dpreview.com
3. DxO Mark               		http://www.dxomark.com
4. Camera Image Sensor    		http://www.cameraimagesensor.com
5. Dave Camera Review      		http://www.imaging-resource.com
6. Adorama                 		http://www.adorama.com


BEST SCREEN CAPTURE DEVICE
1. The Elgado


BEST VIDEO PLAYERS
VLC PLAYER   				http://www.videolan.org/vlc/index.html


BEST SCREEN CAPTURE APPLICATIONS
1. BB Flashback				http://www.bbsoftware.co.uk 
2. Camtasia            		 	http://www.techsmith.com
3. Smartpixel           	  	http://www.smartpixel.com
4. Bandicam             	  	http://www.bandicam.com
5. DXTORY               	  	http://www.dxtory.com
6. Open Broadcast 			http://obsproject.com
7. Mirillis Action      	  	http://Mirillis.com


BEST FREE DEINTERLACING SOFTWARE
1. Handbrake			 	https://trac.handbrake.fr
2. VirtualDub				http://www.virtualdub.org


BEST FREE VIDEO EDITOR
1. DaVinci Resolve - FREE   		https://www.blackmagicdesign.com/products/davinciresolve
2. Hit Film EXpress for FREE    	https://hitfilm.com/express
3. Filmora   				http://filmora.wondershare.com/
4. Cinelerra for Linux  		http://cinelerra.org/1/

ONLINE VIDEO EDITORS
1. Magisto   				https://www.magisto.com/create


BEST COMMERCIAL VIDEO EDITORS
1. Adobe Premiere CS5
2. Adobe After Effects 
3. Hit Film 2.0
4. Sony Vegas 13


BEST CAMERA TRACKING SOFTWARE
1. SynthEyes Pro 64 		 	Http://www.ssontech.com
2. Boujou


BEST FREE HDR SOFTWARE AND IMAGES
1. sIBL GUI 4 - HDRlabs (free)  	http://www.hdrlabs.com



BEST PLUGINS FOR ADOBE AE and PREMIER
1. Color Finesse   		  	http://www.syntheticaperture.com/pages/free-trial
2. Saber 			 	http://www.videocopilot.com
3. Magic Bullet Suite			http://www.redgiant.com/product-downloads/?os=Windows


ART SOFTWARE
1. Krita 2.7
2. Inkscape
3. Vector Magic
4. My Paint
5. GIMP                     		http://www.gimp.org
6. Spriter                  		http://www.brashmonkey.com/download_spriter.htm
7. Crazy Talk Animation     	 	http://crazytalk.reallusion.com/
8. Vector Magic             	  	http://vectormagic.com/home


BEST 3D MODELER/ANIMATION
 1. Modo         		  	http://www.thefoundry.co.uk/products/modo/
 2. Zbrush       		  	http://www.pixologic.com
 3. Cinema 4D    		  	http://www.Maxon.net
 4. Lightwave    		  	http://www.newtek.com
 5. Houdini      		  	http://www.sidefx.com
 6. Blender      		  	http://www.blender.org 
 7. MakeHuman    		  	http://www.makehuman.org
 8. Messiah Studio  		 	http://www.projectmessiah.com/x6/index.html
 9. 3D Coat  			  	http://3d-coat.com
10. 3D Studio Max  		  	http://www.autodesk.com

Best Bump Mapping Applications
1. Substance Painter		  	http://www.allegorithmic.com/products/substance-painter
2. Marmoset Toolbag  		 	http://marmoset.co/toolbag
3. Crazybump   			  	http://www.crazybump.com
4. Xnormal     			  	http://www.xnormal.com




Boot Time Defrag by Puran
http://www.puransoftware.com/index.html


Best Gaming Hardware Debate 
www.game-debate.com

Photoshop Training
1. http://photoshoptrainingchannel.com/


Anti Virus Benchmark Testing
1. AV-Test      	  		http://www.av-test.org


  