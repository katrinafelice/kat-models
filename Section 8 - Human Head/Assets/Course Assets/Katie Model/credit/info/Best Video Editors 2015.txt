
1. DaVinci Resolve - FREE  
https://www.blackmagicdesign.com/products/davinciresolve

DaVinci Resolve 12.5 combines professional non-linear video editing with the world�s most advanced color corrector so now you can edit, color correct, finish and deliver all from one system! DaVinci Resolve is completely scalable and resolution independent so it can be used on set, in a small studio or integrated into the largest Hollywood production pipeline! From creative editing and multi camera television production to high end finishing and color correction, only DaVinci Resolve features the creative tools, compatibility, speed and legendary image quality you need to manage your entire workflow, which is why it is the number one solution used on Hollywood feature films.   


2. Hit Film EXpress for FREE  
https://hitfilm.com/express

Making videos used to be really expensive. Now you get to do it all for free in HitFilm 4 Express: editing and visual effects.  In our first tutorial we show just how easy it is to create Iron Man's iconic heads-up display using entirely free software.  
Nobody likes masking, especially if you have to rotoscope every frame. In HitFilm 3 Pro the entire masking system has been reworked to be faster, with mask tracking courtesy of the expanded mocha HitFilm.


2. Adobe Premiere Pro
Adobe is acclaimed worldwide for its impressive software tools, many of which have become true industry standards in different fields and Adobe Premiere Pro is one of it's flagship products.

3. Adobe After Effects
After Effects is the most versatile visual effects package on the market. Straight out of the box, it comes with an assortment of tools, plugins and presets to help build your project.    

4.Media Composer
Media Composer is the most proven video editing software in the industry, trusted by professional editors in every segment of movie making, television, broadcast, and streaming media.

5. Sony Vegas Pro
Vegas Pro 13 takes giant steps toward making the total multimedia creation experience faster and easier than ever. In addition to technological advancements that span the production process from ingesting to editing to archiving, the Vegas Pro Connect iPad app provides several truly unique collaborative review process options that elevate client and team member participation to an entirely new level.

6. Magix Video Pro
MAGIX Video Pro X6 is a video editing solution geared towards the unique requirements of ambitious and professional users. The multi-award-winning editing suite features an unrivaled range of powerful tools for detailed video and audio editing.

7. Edius Pro
EDIUS Pro 7 is the fastest and most versatile real-time editing software � 4K, 3D, HD, SD and almost any format from 24x24 to 4Kx2K, all on the same timeline, even in nested sequences, all in real-time. EDIUS Pro 7 gives you the ability to handle more resolutions, unlimited tracks and real-time editing for the ability to Edit Anything, Anywhere. EDIUS Pro 7 is the perfect finishing tool for studio, news, corporate, documentary and 4K theatrical productions.

8.Corel video studio
With a bold new 64-bit architecture, including a comprehensive 64-bit premium special effects pack and seven powerful FX applications, Corel VideoStudio Ultimate X7 is the easiest and most powerful VideoStudio ever.

